import React, {useState} from 'react'
import { Button, Form } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { loginUser } from '../../redux/user/user.actions'
import Alert from '../../components/alert/alert.component'
import { AppStateType } from '../../redux/store'

import './login.styles.scss'
type UserInfo = {
    email:string
    password:string    
}
type LoginProps = {
    loginUser(userInfo:UserInfo):void
    error:any
}

const Login:React.FC<LoginProps> = ({loginUser, error}) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handlers = {    
    email: setEmail,
    password: setPassword
  }
  type Handlers = typeof handlers
  type Field = keyof Handlers  

  const onChangeHandler = (event:React.ChangeEvent<HTMLInputElement>, field:Field) => {
    handlers[field](event.target.value)
  }

  const onSubmitHandler = () => {
      const userInfo:UserInfo = {email, password}            
      loginUser(userInfo)
  }

    return (            
        <div className="login-container">            
            <Form className='login-form' onSubmit={onSubmitHandler}>
                <Form.Field>
                <label>Email</label>
                <input 
                    placeholder='Email' 
                    value={email}
                    onChange={(event) => onChangeHandler(event, 'email')}
                />
                </Form.Field>
                <Form.Field>
                <label>Password</label>
                <input 
                    placeholder='Password'
                    value={password}
                    onChange={(event) => onChangeHandler(event, 'password')}
                />
                </Form.Field>            
                <Button type='submit'>Submit</Button>
            </Form>
            {error ? <Alert message={error.message}/> : null}
        </div>               
    )
}

const mapStateToProps = (state:AppStateType) => ({
    error: state.user.authError
})

export default connect(mapStateToProps, {loginUser})(Login)