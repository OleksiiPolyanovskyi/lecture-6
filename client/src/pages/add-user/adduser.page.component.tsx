import React from 'react'
import { connect } from 'react-redux'
import AddUser from '../../components/addUser/addUser.component'
import UserHeader from '../../components/userpage-header/userheader.component'
import { addUser, updateUser } from '../../redux/user/user.actions'
import { User } from '../../interfaces/interfaces'
import { useHistory } from "react-router-dom"
import { AppStateType } from '../../redux/store'
import  Alert from '../../components/alert/alert.component'

import './adduser.page.styles.scss'

type AddUserPageProps = {
  userForUpdate:User | null
  addUser(user:User):void
  updateUser(user:User):void
  error:any
}

const AddUserPage:React.FC<AddUserPageProps> = ({userForUpdate, addUser, updateUser, error}) => {
  let history = useHistory()
  const addUserHandler = (user:User) => {
    addUser(user)
    history.push('/list')
  }
  const updateUserHandler = (user:User) => {
    updateUser(user)
    history.push('/list')
  }

    return(
        <>
          <UserHeader />
          <div className="add-user">
            <AddUser
              user={userForUpdate}
              addUserHandler={addUserHandler} 
              updateUserHandler={updateUserHandler}
            />
          </div>
          {error ? <Alert message={error.message}/> : null}      
        </>
    )
}

const mapStateToProps = (state:AppStateType) => ({
    userForUpdate:state.user.userForUpdate,
    error:state.user.error
})

export default connect(mapStateToProps, {addUser, updateUser})(AddUserPage)