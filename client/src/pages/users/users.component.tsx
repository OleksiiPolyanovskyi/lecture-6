import React, {useEffect } from 'react'
import { useHistory } from "react-router-dom"
import { fetchUserStart, setUserForUpdate, deleteUser } from '../../redux/user/user.actions'
import { connect } from 'react-redux'
import { AppStateType } from '../../redux/store'
import { User } from '../../interfaces/interfaces'
import  UserList  from '../../components/user-list/userList.component'
import Spinner from '../../components/spinner/spinner.component'
import UserHeader from '../../components/userpage-header/userheader.component'
import Alert from '../../components/alert/alert.component'
import { Button } from "semantic-ui-react"

import './users.styles.scss'

type UsersPropsType = {
    users:User[]
    loading:boolean
    fetchUserStart():void
    setUserForUpdate(user:User):void
    deleteUser(id:string):void
    error:any
}

const Users:React.FC <UsersPropsType> = ({
    users,
    loading,
    fetchUserStart,
    setUserForUpdate,
    deleteUser,
    error
}) => {
    useEffect(()=>{
        fetchUserStart()
    },[])

    let history = useHistory()

    return(
        <div className="users_container">
            {loading ? 
            <Spinner /> : 
            <div>
                <UserHeader />
                <div className="users_field">                    
                   <UserList 
                     users={users}
                     setUserForUpdate={setUserForUpdate}
                     deleteUser={deleteUser}
                    />               
                </div>  
                <Button 
                    color="purple" 
                    className="add-button" 
                    onClick={()=> history.push('/add')}
                >
                    ADD USER
                </Button>
                <Button 
                    color="purple" 
                    className="chat-button" 
                    onClick={()=> history.push('/')}
                >
                    CHAT
                </Button>
            </div>
            }
            {error ? <Alert message={error.message}/> : null}
        </div>       
    )
}

const mapStateToProps = (state:AppStateType) => ({
    users: state.user.users,
    loading: state.user.loading,
    error: state.user.error
})

const actions = {
    fetchUserStart,
    setUserForUpdate,
    deleteUser
}

export default connect(mapStateToProps, {...actions})(Users)