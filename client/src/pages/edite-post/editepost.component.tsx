import React from 'react'
import AddPost from '../../components/addpost/add.component'
import { Post } from '../../interfaces/interfaces'
import { connect } from 'react-redux'
import { AppStateType } from '../../redux/store'
import { updatePost } from '../../redux/post/post.actions'
import Alert from '../../components/alert/alert.component'

import './editepost.styles.scss'

type UpdatePostPageProps = {
    postForUpdate:Post | null
    updatePost(post:Post):void
    error:any  
}

const UpdatePostPage:React.FC <UpdatePostPageProps> = ({postForUpdate, updatePost, error }) => {  
    return(  
        <div className='edit-post_container'>
            <AddPost post={postForUpdate} updatePost={updatePost}/>
            {error ? <Alert message={error.message}/> : null}
        </div>
)}

const mapStateToProps = (state:AppStateType) => ({
    postForUpdate: state.posts.postForUpdate,
    error: state.posts.error
})

export default connect(mapStateToProps, {updatePost})(UpdatePostPage)