import React, { useEffect, useRef } from 'react'
import { useHistory } from 'react-router-dom'
import{ Post, User } from '../../interfaces/interfaces'
import Header from '../../components/header/header.component'
import AddPost from '../../components/addpost/add.component'
import PostsBlock from '../../components/posts-block/postsBlock.component'
import  Spinner  from '../../components/spinner/spinner.component'
import Alert from '../../components/alert/alert.component'
import { getUsersQuantity } from '../../helpers/userActions'
import { getLatestDate, changeDataFormatForBlocks, simplifyDateFormat } from '../../helpers/propsTransformers'
import { createPost } from '../../helpers/postsOperations'
import { connect } from 'react-redux'
import { fetchPostsStart, addPost } from '../../redux/post/post.actions'
import { AppStateType } from '../../redux/store'
import { Button } from "semantic-ui-react"
import './chat.styles.scss'

type ChatProps = {  
  user:User | null,
  loading:boolean
  fetchPostsStart():void   
  addPost(post:Post):void  
  posts:Post[]
  postForUpdate:Post | null
  error:any
}


const Chat:React.FC <ChatProps> = ({
  posts, 
  user,
  postForUpdate,
  loading,
  fetchPostsStart,   
  addPost,
  error
}) => {

  useEffect(()=>{
    fetchPostsStart()
  },[])
  const listBottom = useRef<HTMLDivElement>(null)
  const history = useHistory()

  let usersCount:number = 0
  let messagesCount:number = 0
  let lastDate:string = ''
  let sipmlifyedPosts:Post[]
  let postsInBlockFormat:any
  
  if (posts.length > 0){
    usersCount = getUsersQuantity(posts)
    messagesCount = posts.length
    lastDate = getLatestDate(posts)
    sipmlifyedPosts = simplifyDateFormat(posts)
    postsInBlockFormat = changeDataFormatForBlocks(sipmlifyedPosts) 
  }

 const addPostHandler = (text:string) => {    
    const newPost:Post = createPost(text, user)       
    addPost(newPost)
    setTimeout(()=>{
      listBottom.current?.scrollIntoView()
    },500)    
  }
    
  return (
    <div className="app_container">
     {loading ? 
      <Spinner /> : 
      <div className="chat_page">
        <div className="chat_container" >
            <Header        
              usersCount={usersCount} 
              messagesCount={messagesCount} 
              lastDate={lastDate}
            />
            <div className="posts_field">
                  {Object.keys(postsInBlockFormat)
                    .map((date, i) => (
                    <PostsBlock 
                      key={i} 
                      date={date} 
                      posts={postsInBlockFormat[date]} 
                      user={user}                                     
                    />))}
                  <div ref={listBottom}></div>
            </div>      
        </div>
        <AddPost addPost={addPostHandler}/>        
      </div>      
    }
    {error ? <Alert message={error.message}/> : null}
    { user?.role === 'admin' ? 
      <Button 
        color="purple" 
        className="list-button" 
        onClick={()=> history.push('/list')}
      >USERS LIST</Button> :
      null
    }
  </div>
 )
}

const mapStateToProps = (state:AppStateType) => ({
  loading:state.posts.loading,
  posts:state.posts.posts,
  postForUpdate:state.posts.postForUpdate,
  error:state.posts.error,
  user:state.user.currentUser
})

const actions = {
  fetchPostsStart,
  addPost 
}

export default connect(mapStateToProps, {...actions})(Chat)
