const NUMBER_OF_MS_DAY = 86400000
const NUMBER_OF_MS_TWO_DAYS = 172800000


function getDateFormat(date){
  const postDate = new Date(date)
  const day = postDate.toLocaleString('en-EN', { weekday: 'long' })
  const calDate = postDate.getDate()
  const month = postDate.toLocaleString('en-EN', { month: 'long' })
  return `${day} ${calDate} ${month}`
}

export function getDateFormatForBlocks(date){
  const now = new Date()
  const postDate = new Date(date)
  const period = now.getTime() - postDate.getTime()  
  if (period < NUMBER_OF_MS_DAY) {
    return 'Today'
  } else if (period < NUMBER_OF_MS_TWO_DAYS) {
    return 'Yesterday'
  } else {
    return getDateFormat(date)
  }
}

export function createDateForHeader(date){
  const last = getDateFormat(date)  
  const postDate = new Date(date)
  const hours = postDate.getHours()
  const minutes = postDate.getMinutes()
  return `${hours}:${minutes} ${last}`
}