import { v4 as uuidv4 } from 'uuid'

export function createCurrentUser(){
  return {
    id: uuidv4(),
    name:'current user (you)'
  }
}

export function getUsersQuantity(data){
  const users = new Set()
  data.forEach(post => users.add(post.userId))
  return users.size
}