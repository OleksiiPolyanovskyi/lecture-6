export function removeFromCollection(id, collection){
    return collection.filter(item => item.id !== id)
  }
  
  export function updateCollection(element, collection){
    const copy = [...collection]
    const index = copy.findIndex(item => item.id === element.id)
    copy.splice(index, 1, element)
    return copy
  }