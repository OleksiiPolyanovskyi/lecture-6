import { v4 as uuidv4 } from 'uuid'

export function createPost(text, currentUser){
  return {
    id: uuidv4(),
    userId: currentUser.id,    
    user: currentUser.name,
    text: text,
    createdAt: new Date().toISOString(),
    editedAt: '',
    likesCount: new Set()
  }
}

export function likePost(post, posts, userId){  
  const copy = [...posts]
  const index = copy.findIndex(item => item.id === post.id)
  const updated = copy[index]
  updated.likesCount?.add(userId)
  copy.splice(index, 1, updated) 
  return copy 
}