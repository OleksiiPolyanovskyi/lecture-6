import { get, post, deleteReq, put } from "../requestHelper"

const entity = 'posts'
const likeEntity = 'posts/like'

export const getPosts = async () => {
    return await get(entity)
}

export const createPost = async (body) => {
    return await post(entity, body)
}

export const deletePost = async (id) => {
    return await deleteReq(entity, id)
}

export const updatePost = async (id, body) => {
    return await put(entity, id, body)
}

export const likePost = async (id, body) => {
    return await put(likeEntity, id, body)
}