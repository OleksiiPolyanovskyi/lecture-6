import { post, get, deleteReq, put } from "../requestHelper";
const entity = 'users'

export const createUser = async (body) => {
    return await post(entity, body);
}

export const getAllUsers = async () => {      
    return await get(entity)
}

export const deleteUser = async (id) => {
    return await deleteReq(entity, id)
}

export const updateUser = async (id, body) => {
    return await put(entity, id, body)
}