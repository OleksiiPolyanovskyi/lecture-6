import React, {useEffect} from 'react'
import { User } from './interfaces/interfaces'
import Chat from './pages/chat/chat.component'
import Users from './pages/users/users.component'
import AddUserPage from './pages/add-user/adduser.page.component'
import UpdatePostPage from './pages/edite-post/editepost.component'
import Login from './pages/login/login.component'
import { Switch, Route } from 'react-router-dom'
import PrivateRoute from './components/protected-route/protected.component'
import PublicRoute from './components/public-route/public.component'
import AdmineRoute from './components/admin-route/admin.component'
import { connect } from 'react-redux'
import { loginUserSuccess } from './redux/user/user.actions'
import { getUser } from './services/authService'
import './App.scss'

type Props = {  
  login(user:User):void
}

const App:React.FC <Props> = ({login}) => {  
  useEffect(()=>{
    const user = getUser()
    if(user) login(user)
  })
  return (  
  <Switch>
    <PublicRoute exact path = '/login' component = {Login} />
    <PrivateRoute exact path = '/' component={Chat} />   
    <PrivateRoute exact path="/chat/edit/:id" component={UpdatePostPage} />
    <AdmineRoute exact path = '/list' component={Users} />    
    <AdmineRoute exact path="/add" component={AddUserPage} />
    <AdmineRoute exact path="/edit/:id" component={AddUserPage} />    
  </Switch>
)}

export default connect(null, {login: loginUserSuccess})(App)
