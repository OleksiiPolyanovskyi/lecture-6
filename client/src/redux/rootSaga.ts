import { all } from 'redux-saga/effects'

import { fetchPostsStart, addPost, deletePost, updatePost, likePost } from './post/post.saga'
import { fetchUsersStart, addUser, updateUser, deleteUser, loginUser } from './user/user.saga'

export default function* rootSaga(){
    yield all([
        fetchPostsStart(),
        addPost(),
        deletePost(),
        updatePost(),
        likePost(),
        fetchUsersStart(),
        addUser(),
        updateUser(),
        deleteUser(),
        loginUser()
    ])
}