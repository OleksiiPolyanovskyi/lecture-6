import {
    FETCH_USERS_START,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FAILURE,
    ADD_USER,
    ADD_USER_SUCCESS,
    ADD_USER_FAILURE,
    UPDATE_USER,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_FAILURE,
    DELETE_USER,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAILURE,
    SET_USER_FOR_UPDATE,
    REM_USER_FROM_UPDATE,
    LOGIN,
    LOGIN_SUCCESS,
    LOGIN_FAILURE
} from './user.action.types'
import { User } from '../../interfaces/interfaces'

type FetchUserStartType = {
    type: typeof FETCH_USERS_START
}
export const fetchUserStart = ():FetchUserStartType => ({
    type:FETCH_USERS_START
})

type FetchUserSuccessType = {
    type: typeof FETCH_USERS_SUCCESS
    payload:User[]
}
export const fetchUserSuccess = (users:User[]):FetchUserSuccessType => ({
    type:FETCH_USERS_SUCCESS,
    payload:users
})

type FetchUserFailureType = {
    type: typeof FETCH_USERS_FAILURE
    payload:any
}
export const fetchUserFailure = (error:any):FetchUserFailureType => ({
    type:FETCH_USERS_FAILURE,
    payload:error
})

///ADD
export type AddUserType = {
    type: typeof ADD_USER
    payload:User
}
export const addUser = (user:User):AddUserType => ({
    type:ADD_USER,
    payload:user
})

type AddUserSuccessType = {
    type: typeof ADD_USER_SUCCESS
    payload:User
}
export const addUserSuccess = (user:User):AddUserSuccessType => ({
    type:ADD_USER_SUCCESS,
    payload:user
})

type AddUserFailureType = {
    type: typeof ADD_USER_FAILURE
    payload:any
}
export const addUserFailure = (error:any):AddUserFailureType => ({
    type:ADD_USER_FAILURE,
    payload:error
})

///UPDATE

export type UpdateUserType = {
    type: typeof UPDATE_USER
    payload:User
}
export const updateUser = (user:User):UpdateUserType => ({
    type:UPDATE_USER,
    payload:user
})

type UpdateUserSuccessType = {
    type: typeof UPDATE_USER_SUCCESS
    payload:User
}
export const updateUserSuccess = (user:User):UpdateUserSuccessType => ({
    type:UPDATE_USER_SUCCESS,
    payload:user
})

type UpdateUserFailureType = {
    type: typeof UPDATE_USER_FAILURE
    payload:any
}
export const updateUserFailure = (error:any):UpdateUserFailureType => ({
    type:UPDATE_USER_FAILURE,
    payload:error
})

///DELETE

export type DeleteUserType = {
    type: typeof DELETE_USER
    payload:string
}
export const deleteUser = (id:string):DeleteUserType => ({
    type:DELETE_USER,
    payload:id
})

type DeleteUserSuccessType = {
    type: typeof DELETE_USER_SUCCESS
    payload:string
}
export const deleteUserSuccess = (id:string):DeleteUserSuccessType => ({
    type:DELETE_USER_SUCCESS,
    payload:id
})

type DeleteUserFailureType = {
    type: typeof DELETE_USER_FAILURE
    payload:any
}
export const deleteUserFailure = (error:any):DeleteUserFailureType => ({
    type:DELETE_USER_FAILURE,
    payload:error
})

type SetUserForUpdateType = {
    type: typeof SET_USER_FOR_UPDATE
    payload:User
}
export const setUserForUpdate = (user:User):SetUserForUpdateType => ({
    type:SET_USER_FOR_UPDATE,
    payload:user
})

type RemUserFromUpdateType = {
    type: typeof REM_USER_FROM_UPDATE
   
}
export const remUserFromUpdate = ():RemUserFromUpdateType => ({
  type:REM_USER_FROM_UPDATE  
})

type UserInfo = {
    email:string
    password:string
}

export type LoginType = {
    type: typeof LOGIN
    payload:UserInfo   
}
export const loginUser = (user:UserInfo):LoginType => ({
  type:LOGIN,
  payload:user 
})

type LoginTypeSuccess = {
    type: typeof LOGIN_SUCCESS
    payload:User
}
export const loginUserSuccess = (user:User):LoginTypeSuccess => ({
  type:LOGIN_SUCCESS,
  payload:user  
})

type LoginTypeFailure = {
    type: typeof LOGIN_FAILURE
    payload:any
}
export const loginUserFailure = (error:any):LoginTypeFailure => ({
  type:LOGIN_FAILURE,
  payload:error  
})



export type UserActionsType = FetchUserStartType | FetchUserSuccessType | FetchUserFailureType |
  AddUserType | AddUserSuccessType | AddUserFailureType | UpdateUserType | UpdateUserSuccessType |
  UpdateUserFailureType | DeleteUserType | DeleteUserSuccessType | DeleteUserFailureType | RemUserFromUpdateType |
  SetUserForUpdateType | LoginTypeSuccess | LoginTypeFailure