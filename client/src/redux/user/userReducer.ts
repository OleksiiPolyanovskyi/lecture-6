import { createCurrentUser } from '../../helpers/userActions'
import { User } from '../../interfaces/interfaces'
import { UserActionsType } from './user.actions'
import {
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FAILURE,
    ADD_USER_SUCCESS,
    ADD_USER_FAILURE,    
    UPDATE_USER_SUCCESS,
    UPDATE_USER_FAILURE,    
    DELETE_USER_SUCCESS,
    DELETE_USER_FAILURE,
    SET_USER_FOR_UPDATE,
    REM_USER_FROM_UPDATE,
    LOGIN_SUCCESS,
    LOGIN_FAILURE
} from './user.action.types'
import { removeFromCollection, updateCollection } from '../../helpers/generalOperations'

const initialState:InitialState = {
    currentUser:null,
    users:[],
    loading:true,
    error:null,
    userForUpdate:null,
    authError:null
}

type InitialState = {
    currentUser:User | null
    users:User[]
    loading:boolean
    error:any
    userForUpdate:User | null
    authError:any
}
 
export default (state = initialState, action:UserActionsType):InitialState => {
    switch(action.type){
        case FETCH_USERS_SUCCESS:
        return {
            ...state,
            users:action.payload,
            loading:false,
            error:null
        }
        case FETCH_USERS_FAILURE:
        return {
            ...state,
            error: action.payload,
            loading:false
        }
        case ADD_USER_SUCCESS:
        return {
            ...state,
            users:[...state.users, action.payload],
            error:null
        }
        case ADD_USER_FAILURE:
        return {
            ...state,
            error: action.payload
        }
        case DELETE_USER_SUCCESS:
        return {
            ...state,
            users: removeFromCollection(action.payload, state.users),
            error:null
        }
        case DELETE_USER_FAILURE:
        return {
            ...state,
            error:action.payload
        }
        case UPDATE_USER_SUCCESS:
        return {
            ...state,
            users: updateCollection(action.payload, state.users),
            userForUpdate:null,
            error:null
        }
        case UPDATE_USER_FAILURE:
        return {
            ...state,
            error:action.payload            
        }
        case SET_USER_FOR_UPDATE:
        return {
            ...state,
            userForUpdate:action.payload           
        }
        case REM_USER_FROM_UPDATE:
        return {
            ...state,
            userForUpdate:null            
        }
        case LOGIN_SUCCESS:
        return {
            ...state,
            currentUser:action.payload,
            authError:null
        }
        case LOGIN_FAILURE:
        return {
            ...state,
            authError:action.payload
        }
        default: return state
    }      
}
