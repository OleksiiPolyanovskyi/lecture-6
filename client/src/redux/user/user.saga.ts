import { takeEvery, put } from 'redux-saga/effects'
import {
   FETCH_USERS_START,
   ADD_USER,
   UPDATE_USER,
   DELETE_USER,
   LOGIN
} from './user.action.types'
import * as userSevice from '../../services/domainRequest/userRequest'
import { login } from '../../services/domainRequest/auth'
import {
    fetchUserSuccess,
    fetchUserFailure,
    addUserSuccess,
    addUserFailure,
    AddUserType,
    updateUserSuccess,
    updateUserFailure,
    UpdateUserType,
    deleteUserSuccess,
    deleteUserFailure,
    DeleteUserType,
    LoginType,    
    loginUserSuccess,
    loginUserFailure
} from './user.actions'
import { setLoginSession } from '../../services/authService'

function* fetchUsersAsync(){
    try {
        const users = yield userSevice.getAllUsers()                  
        yield put(fetchUserSuccess(users))
    }catch(error){
        yield put(fetchUserFailure(error))
    }  
}

export function* fetchUsersStart(){
    yield takeEvery(
        FETCH_USERS_START,
        fetchUsersAsync
    )
}

function* addUserAsync(action:AddUserType){
    try {              
        const user = yield userSevice.createUser(action.payload)                
        yield put(addUserSuccess(user))
    }catch(error){
        yield put(addUserFailure(error))
    }  
}

export function* addUser(){
    yield takeEvery(
        ADD_USER,
        addUserAsync
    )
}

function* updateUserAsync(action:UpdateUserType){
    try {
        const {id, ...body} = action.payload
        const user = yield userSevice.updateUser(id, body)                
        yield put(updateUserSuccess(user))
    }catch(error){
        yield put(updateUserFailure(error))
    }  
}

export function* updateUser(){
    yield takeEvery(
        UPDATE_USER,
        updateUserAsync
    )
}

function* deleteUserAsync(action:DeleteUserType){
    try {        
        yield userSevice.deleteUser(action.payload)                
        yield put(deleteUserSuccess(action.payload))
    }catch(error){
        yield put(deleteUserFailure(error))
    }  
}

export function* deleteUser(){
    yield takeEvery(
        DELETE_USER,
        deleteUserAsync
    )
}

function* loginAsync(action:LoginType){
    try {             
        const user = yield login(action.payload)
        if (user && user.id){
            yield put(loginUserSuccess(user))
            yield setLoginSession(user) 
        } else {
            yield put(loginUserFailure({message:'User was not found'}))
        }                 
    }catch(error){                
        yield put(loginUserFailure(error))
    }  
}

export function* loginUser(){
    yield takeEvery(
        LOGIN,
        loginAsync
    )
}