import {
    createStore,
    applyMiddleware, 
    compose,   
    combineReducers
  } from 'redux'
import createSagaMiddleware from 'redux-saga'
import postReducer from './post/postReducer'
import userReducer from './user/userReducer'
import rootSaga from './rootSaga'

const sagaMiddleware = createSagaMiddleware()

const middlewares = [sagaMiddleware]

const reducers = {
    posts: postReducer,
    user: userReducer
}

const rootReducer = combineReducers({...reducers})

type RootReducerType = typeof rootReducer
export type AppStateType = ReturnType<RootReducerType>

declare global {
    interface Window {
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
  }
  
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
    rootReducer,    
    composeEnhancers(applyMiddleware(...middlewares))    
)

sagaMiddleware.run(rootSaga)
