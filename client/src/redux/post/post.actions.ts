import { 
    FETCH_POSTS_START, 
    FETCH_POSTS_SUCCESS, 
    FETCH_POSTS_FAILURE,
    ADD_POST,
    ADD_POST_SUCCESS,
    ADD_POST_FAILURE,
    DELETE_POST,
    DELETE_POST_SUCCESS,
    DELETE_POST_FAILURE,
    UPDATE_POST,
    UPDATE_POST_SUCCESS,
    UPDATE_POST_FAILURE,
    LIKE_POST,
    SET_POST_FOR_UPDATE,
    REM_POST_FOR_UPDATE
} from './post.action.types'
import { Post } from '../../interfaces/interfaces'
 
type fetchPostsStartType = {
    type: typeof FETCH_POSTS_START
}
export const fetchPostsStart = ():fetchPostsStartType => ({type:FETCH_POSTS_START})

type fetchPostsSuccessType = {
    type: typeof FETCH_POSTS_SUCCESS
    payload:Post[]
}
export const fetchPostsSuccess = (posts:Post[]):fetchPostsSuccessType => ({
    type:FETCH_POSTS_SUCCESS,
    payload: posts
})

type fethcPostsFailureType = {
    type: typeof FETCH_POSTS_FAILURE
    payload:any
}
export const fethcPostsFailure = (error:any):fethcPostsFailureType => ({
    type: FETCH_POSTS_FAILURE,
    payload: error
})

export type addPostType = {
    type: typeof ADD_POST
    payload:Post
}
export const addPost = (post:Post):addPostType => ({
    type:ADD_POST,
    payload:post
})

type addPostSuccessType = {
    type: typeof ADD_POST_SUCCESS
    payload:Post
}
export const addPostSuccess = (post:Post):addPostSuccessType => ({
    type:ADD_POST_SUCCESS,
    payload:post
})

type addPostFailureType = {
    type: typeof ADD_POST_FAILURE
    payload:any
}
export const addPostFailure = (error:any):addPostFailureType => ({
    type:ADD_POST_FAILURE,
    payload:error
})

export type deletePostType = {
    type: typeof DELETE_POST
    payload:string
}
export const deletePost = (id:string):deletePostType => ({
    type:DELETE_POST,
    payload:id
})

type deletePostSuccessType = {
    type: typeof DELETE_POST_SUCCESS
    payload:string
}
export const deletePostSuccess = (id:string):deletePostSuccessType => ({
    type:DELETE_POST_SUCCESS,
    payload:id
})

type deletePostFailureType = {
    type: typeof DELETE_POST_FAILURE
    payload:any
}
export const deletePostFailure = (error:any):deletePostFailureType => ({
    type:DELETE_POST_FAILURE,
    payload:error
})

export type updatePostType = {
    type:typeof UPDATE_POST
    payload:Post
}
export const updatePost = (post:Post):updatePostType => ({
    type:UPDATE_POST,
    payload:post
})

type updatePostSuccessType = {
    type:typeof UPDATE_POST_SUCCESS
    payload:Post
}
export const updatePostSuccess = (post:Post):updatePostSuccessType => ({
    type:UPDATE_POST_SUCCESS,
    payload:post
})

type updatePostFailureType = {
    type:typeof UPDATE_POST_FAILURE
    payload:any
}
export const updatePostFailure = (error:any):updatePostFailureType => ({
    type:UPDATE_POST_FAILURE,
    payload:error
})

type likePostPayload = {
    post:Post
    userId:string
}
export type likePostType = {
    type:typeof LIKE_POST
    payload:likePostPayload
}
export const likePost = (post:Post, userId:string):likePostType => ({
    type:LIKE_POST,
    payload:{post, userId}
})

type setPostForUpdateType = {
    type:typeof SET_POST_FOR_UPDATE
    payload:Post
}
export const setPostForUpdate = (post:Post):setPostForUpdateType =>({
    type:SET_POST_FOR_UPDATE,
    payload:post
})

type remPostForUpdateType = {
    type:typeof REM_POST_FOR_UPDATE
}
export const remPostForUpdate = ():remPostForUpdateType =>({
    type:REM_POST_FOR_UPDATE    
})

export type PostActionTypes = fetchPostsStartType | fetchPostsSuccessType | fethcPostsFailureType | 
addPostType | deletePostType | updatePostType | likePostType | setPostForUpdateType |
remPostForUpdateType | addPostFailureType | addPostSuccessType | deletePostSuccessType |
deletePostFailureType | updatePostSuccessType | updatePostFailureType 
