import { 
    FETCH_POSTS_START,
    FETCH_POSTS_SUCCESS, 
    FETCH_POSTS_FAILURE,
    ADD_POST_SUCCESS,
    ADD_POST_FAILURE,
    DELETE_POST_SUCCESS,
    DELETE_POST_FAILURE,
    UPDATE_POST_SUCCESS,
    UPDATE_POST_FAILURE,
    LIKE_POST,
    SET_POST_FOR_UPDATE,
    REM_POST_FOR_UPDATE
} from './post.action.types'
import { likePost } from '../../helpers/postsOperations'
import { removeFromCollection, updateCollection } from '../../helpers/generalOperations'
import { Post } from '../../interfaces/interfaces'
import { PostActionTypes } from './post.actions'

const initialState = {
    posts:[] as Post[],
    loading:true,
    postForUpdate:null as Post | null,
    error:null
}

type InitialState = typeof initialState

export default (state=initialState, action:PostActionTypes):InitialState => {
    switch(action.type) {
        case FETCH_POSTS_START:
        return {
            ...state,
            loading:true
        }
        case FETCH_POSTS_SUCCESS:
        return {
            ...state,
            loading:false,
            posts:action.payload,
            error:null
        }
        case FETCH_POSTS_FAILURE:
        return {
            ...state,
            loading:false,
            posts:action.payload
        }
        case ADD_POST_SUCCESS:
        return {
            ...state,
            posts:[...state.posts, action.payload],
            error:null
        }
        case ADD_POST_FAILURE:
        return {
            ...state,
            error: action.payload
        }
        case DELETE_POST_SUCCESS:
        return {
            ...state,
            posts: removeFromCollection(action.payload, state.posts),
            error:null
        }
        case DELETE_POST_FAILURE:
        return {
            ...state,
            error:action.payload
        }
        case UPDATE_POST_SUCCESS:
        return {
            ...state,
            posts: updateCollection(action.payload, state.posts),
            postForUpdate:null,
            error:null
        }
        case UPDATE_POST_FAILURE:
        return {
            ...state,
            error:action.payload            
        }
        case LIKE_POST:
        return {
            ...state,
            posts: likePost(action.payload.post, state.posts, action.payload.userId)
        }
        case SET_POST_FOR_UPDATE:
        return {
            ...state,
            postForUpdate:action.payload            
        }
        case REM_POST_FOR_UPDATE:
        return {
            ...state,
            postForUpdate:null
        }
        default:
        return state
    }
}