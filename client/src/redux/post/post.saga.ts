import { takeEvery, put } from 'redux-saga/effects'
import { 
    FETCH_POSTS_START,
    ADD_POST,
    DELETE_POST,
    UPDATE_POST,
    LIKE_POST
} from './post.action.types' 
import * as postSevice from '../../services/domainRequest/postRequest'
import { 
    fetchPostsSuccess, 
    fethcPostsFailure,
    addPostSuccess,
    addPostFailure,
    addPostType,
    deletePostSuccess,
    deletePostFailure,
    deletePostType,
    updatePostSuccess,
    updatePostFailure,
    updatePostType,
    likePostType
} from './post.actions'
import { sortPosts, changeLikesArrayToSet } from '../../helpers/propsTransformers'
 
function* fetchPostsAsync(){
    try {
        const posts = yield postSevice.getPosts()
        const sorted = yield sortPosts(posts)              
        const likesInSetFormat = yield changeLikesArrayToSet(sorted)              
        yield put(fetchPostsSuccess(likesInSetFormat))
    }catch(error){
        yield put(fethcPostsFailure(error))
    }   
}

export function* fetchPostsStart(){
    yield takeEvery(
        FETCH_POSTS_START,
        fetchPostsAsync
    )
}

function* addPostAsync(action:addPostType){
    try {        
        const post = yield postSevice.createPost(action.payload)
        yield put(addPostSuccess(post))
    }catch(error){
        yield put(addPostFailure(error))
    }   
}

export function* addPost(){
    yield takeEvery(
        ADD_POST,
        addPostAsync
    )
}

function* deletePostAsync(action:deletePostType){
    try {        
        yield postSevice.deletePost(action.payload)
        yield put(deletePostSuccess(action.payload))
    }catch(error){
        yield put(deletePostFailure(error))
    }   
}

export function* deletePost(){
    yield takeEvery(
        DELETE_POST,
        deletePostAsync
    )
}

function* updatePostAsync(action:updatePostType){
    try {
        const {id, ...body} = action.payload     
        const post = yield postSevice.updatePost(id, body)
        yield put(updatePostSuccess(post))
    }catch(error){
        yield put(updatePostFailure(error))
    }   
}

export function* updatePost(){
    yield takeEvery(
        UPDATE_POST,
        updatePostAsync
    )
}

function* likePostAsync(action:likePostType){    
    const {post, userId} = action.payload     
    yield postSevice.likePost(post.id, {userId})       
}

export function* likePost(){
    yield takeEvery(
        LIKE_POST,
        likePostAsync
    )
}