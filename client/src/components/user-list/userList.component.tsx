import React from 'react'
import { User } from '../../interfaces/interfaces'
import { Button, List } from 'semantic-ui-react'
import { useHistory } from "react-router-dom"

type UserPropsType = {
    users:User[]
    setUserForUpdate(user:User):void
    deleteUser(id?:string):void
}

const UserList:React.FC <UserPropsType> = ({users, setUserForUpdate, deleteUser}) => {
    let history = useHistory()
    const editHandler = (user:User) => {
        setUserForUpdate(user)
        history.push(`edit/${user.id}`)
    }
    return (
        <List divided verticalAlign='middle'>
            {users.map(user => {
            return (
                <List.Item>
                <List.Content floated='right'>
                    <Button basic color='purple' onClick={() => editHandler(user)}>Edit</Button>
                    <Button basic color='red' content onClick={() => deleteUser(user.id)}>Delete</Button>
                </List.Content>         
                <List.Content>{user.name}</List.Content>
                <List.Content>{user.email}</List.Content>
                </List.Item> 
            )
            })}       
        </List>
    )
}

export default UserList