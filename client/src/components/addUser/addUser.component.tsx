import React, { useState } from "react"
import { Button, Form, Segment } from "semantic-ui-react"
import { User } from '../../interfaces/interfaces'
import { useHistory } from "react-router-dom"

type addUserProps = {
    user?:User | null
    addUserHandler(user:User):void
    updateUserHandler(user:User):void
}

const AddUser:React.FC <addUserProps> = ({user, addUserHandler, updateUserHandler}) => {
  const [name, setName] = useState(user ? user.name : "")
  const [email, setEmail] = useState(user ? user.email : "")
  const [password, setPassword] = useState(user ? user.password : "")

  const handlers = {
    name: setName,
    email: setEmail,
    password: setPassword
  }
  type Handlers = typeof handlers
  type Field = keyof Handlers

  let history = useHistory()

  const onChangeHandler = (event:React.ChangeEvent<HTMLInputElement>, field:Field) => {
    handlers[field](event.target.value)
  }

  const onSubmitHandler = () => {
    const newUser:User = { name, email, password, role: 'user' }
    if (user) {
      newUser.id = user.id
      updateUserHandler(newUser)
    } else {
      addUserHandler(newUser)      
    }
    (Object.keys(handlers) as Array<Field>).forEach((key) => handlers[key](""))
  };
 
  return (
    <Segment inverted>
      <Form inverted onSubmit={onSubmitHandler}>
        <Form.Field>
          <label>Name</label>
          <input
            placeholder="Name"
            value={name}
            onChange={(event) => onChangeHandler(event, "name")}
          />
        </Form.Field>
        <Form.Field>
          <label>Email</label>
          <input
            placeholder="Email"
            type="email"
            value={email}
            onChange={(event) => onChangeHandler(event, "email")}
          />
        </Form.Field>
        <Form.Field>
          <label>Password</label>
          <input
            placeholder="Password"
            value={password}
            onChange={(event) => onChangeHandler(event, "password")}
          />
        </Form.Field>
        <Button type="submit" inverted color="purple">
          {user ? 'Edit' : 'Add'}
        </Button>
        <Button inverted color="red" onClick={() => history.push('/list')}>
          Cancel
        </Button>
      </Form>
    </Segment>
  )
}

export default AddUser