import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { AppStateType } from '../../redux/store'
import { User } from '../../interfaces/interfaces'

type PrivateRouteProps = {
    currentUser:User | null
    component:any
    exact:any
    path:string
}

const PublicRoute:React.FC<PrivateRouteProps> = ({ component: Component, currentUser, ...rest }) => {
  const path = currentUser?.role === 'admin' ? '/list' : '/'
  return (
    <Route
      {...rest}
      render={props => (currentUser
          ? <Redirect to={{ pathname: path, state: { from: props.location } }} />
          : <Component {...props} />)}
    />
  )
}

const mapStateToProps = (state:AppStateType) => ({
  currentUser: state.user.currentUser
});

export default connect(mapStateToProps)(PublicRoute);