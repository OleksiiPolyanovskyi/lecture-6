import React, {useState} from 'react'
import { Label, Icon } from 'semantic-ui-react';
import { Post, User } from '../../interfaces/interfaces'
import { connect } from 'react-redux'
import { deletePost, likePost, setPostForUpdate } from '../../redux/post/post.actions'
import { useHistory } from 'react-router-dom'

import './post.styles.scss';

type PostProps = {
  post:Post
  currentUser:User | null  
  likePost(post:Post, userId:string):void
  deletePost(id:string):void
  setPostForUpdate(post:Post):void
}

const PostComponent:React.FC<PostProps> = ({
  post,
  currentUser,  
  likePost,
  deletePost,
  setPostForUpdate
}) => {  
  const {
    id,
    userId,
    avatar,
    user,
    text,
    createdAt,
    editedAt,
    likesCount
  } = post
  const [showControls, setShowControls] = useState(false) 
  const date = createdAt
  const selfPostClass = currentUser && userId === currentUser.id ? 'self-post' : ''
  const cardClasses = ['card', selfPostClass] 
  let history = useHistory()
  
  const likePostHandler = (post:Post) => {        
    if (post.userId === currentUser?.id) return 
    if (currentUser) {
      const userId = currentUser.id   
      if (userId) likePost(post, userId) 
    }    
  }
  const deletePostHandler = (id:string) => {
    deletePost(id)
  }

  const updatePostHandler = (post:Post) => {
    setPostForUpdate(post)
    history.push(`/chat/edit/${post.id}`)
  }
  return (
    <div 
      className={cardClasses.join(' ')} 
      onMouseEnter={() => setShowControls(true)}
      onMouseLeave={() => setShowControls(false)}
    >              
      <div className="card_content">
        <div className="meta_block">                  
          <span className="date">
            posted by
            {' '}
            {user}
            {' - '}
            {date}
          </span>
        </div>
        <div className="description_container">
          <div className="image_block">
            {avatar && <img src={avatar} className="post_image"/>} 
          </div>
          <div className="text_block">
            {text}
          </div>                
        </div>
      </div>
         
      <div className="align-left icons_block">        
        <Label basic size="small" as="a" className="toolbarBtn" onClick={()=>likePostHandler(post)}>
          <Icon name="thumbs up" />
          {likesCount?.size}
        </Label>       
        
        {currentUser && userId === currentUser.id && showControls
          ? (
            <>
              <Label basic size="small" as="a" className="toolbarBtn" onClick={()=>updatePostHandler(post)}>
                <Icon name="edit" />
              </Label>
              <Label basic size="small" as="a" className="toolbarBtn" onClick={()=>deletePostHandler(post.id)}>
                <Icon name="remove" />
              </Label>
            </>
          ) : null }
      </div>
    </div>
  );
};

const actions = {
  deletePost,
  likePost,
  setPostForUpdate
}

export default connect(null, {...actions})(PostComponent)