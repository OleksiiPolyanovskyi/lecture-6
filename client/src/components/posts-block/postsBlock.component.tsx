import React from 'react'
import { Post, User } from '../../interfaces/interfaces'
import PostComponent from '../post/post.component'
import { getDateFormatForBlocks } from '../../helpers/dateFormating'
import './postsBlock.styles.scss'


type PostsBlockProps = {
  date:string
  posts:Post[]
  user:User | null   
}

const PostsBlock:React.FC<PostsBlockProps> = ({date, posts, user }) => {
  const formatedDate = getDateFormatForBlocks(date)
  return(
    <div className="block_container">
        <h4 className="block_heading">{formatedDate}</h4>
        <div className="posts_container">
            {posts.map(post => (
            <PostComponent 
              key={post.id} 
              post={post} 
              currentUser={user}                            
            />))}
        </div>
    </div>
  ) 
}

export default PostsBlock