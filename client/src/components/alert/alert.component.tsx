import React from 'react'

import './alert.styles.scss'

type AlertProps = {
    message?:string
}

const Alert:React.FC<AlertProps> = ({message}) => {
    return(        
        <div className="alert__body error">
            <div className="alert__icon error-icon"><h2>!</h2></div>
            <div className="alert__text">
                <h2 className="alert__text-row">Error</h2>
                <span className="alert__text-row">{message}</span>
                <span className="alert__text-row">Please try again later</span>
            </div>
        </div>
    )
}

export default Alert