import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { AppStateType } from '../../redux/store'
import { User } from '../../interfaces/interfaces'

type PrivateRouteProps = {
    currentUser:User | null
    component:any
    exact:any
    path:string
}

const PrivateRoute:React.FC<PrivateRouteProps> = ({ component: Component, currentUser, ...rest }) => (
  <Route
    {...rest}
    render={props => (currentUser
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
  />
)

const mapStateToProps = (state:AppStateType) => ({
  currentUser: state.user.currentUser
});

export default connect(mapStateToProps)(PrivateRoute);