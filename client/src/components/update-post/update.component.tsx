import React from 'react'
import { Modal } from 'semantic-ui-react'
import AddPost from '../addpost/add.component'
import { Post } from '../../interfaces/interfaces'

type UpdatePostProps = {
    post:Post
    updatePost(post:Post):void 
    closeModal():void
}

const UpdatePost:React.FC <UpdatePostProps> = ({post, updatePost, closeModal}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => closeModal()}>
    <Modal.Content>
      <AddPost post={post} updatePost={updatePost}/>
    </Modal.Content>      
  </Modal>
)


export default UpdatePost