import React, { useState } from 'react'
import { Post } from '../../interfaces/interfaces'
import { Form, Button, Segment, Label } from 'semantic-ui-react'
import { useHistory } from 'react-router-dom'

import './add.styles.scss'

type AddPostProps = {
  post?:Post | null  
  addPost?(text:string):void  
  updatePost?(post:Post | null):void
}

const AddPost:React.FC <AddPostProps> = ({
  post=null,  
  addPost,
  updatePost,  
}) => {
  const initialText = post ? post.text : ''
  const [text, setText] = useState(initialText)
  let history = useHistory()
 
  const addPostHandler = () => {
    if (!text) {
      return
    }
    if (post) {
      const updated = {...post, text:text}                
      if(updatePost) updatePost(updated)
      history.push('/')     
    } else {
      if (addPost) addPost(text)
      setText('')      
    }
  }  

  return (
    <Segment>
      <Form onSubmit={addPostHandler}>
        <Form.TextArea
          name="text"
          value={text}
          placeholder="What is the news?"
          onChange={ev => setText(ev.target.value)}
        />        
        <div className="buttonsBlock">         
          <Button floated="right" color="blue" type="submit">{post ? 'Edit' : 'Send'}</Button>
          {post ? <Button floated="right" color="red" onClick={() => history.push('/')}>Cancel</Button> : null}         
        </div>
      </Form>
    </Segment>
  );
};

export default AddPost;