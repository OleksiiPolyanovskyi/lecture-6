const responseMiddleware = (req, res, next) => {   
    if (res.err) {
        res.status(res.err.status || 400).send({error: res.err.message})
    }
    res.status(200).send(res.data)
    next();
}

exports.responseMiddleware = responseMiddleware;