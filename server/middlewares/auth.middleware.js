const authMiddleware = (req, res, next) => {    
    if (!req.body.email || ! req.body.password) {
        const err = new Error()
        err.status = 404;
        err.message = `Please provide all credentials`;
        res.err = err                
    }   
    next();
 }
 
 exports.authMiddleware = authMiddleware;