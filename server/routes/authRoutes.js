const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { authMiddleware } = require('../middlewares/auth.middleware')

const router = Router();

router.post('/login', authMiddleware, ( req, res, next) => {
    try {
        const data = AuthService.login(req.body);            
        const userData = {
            id: data.id, 
            name: data.name,
            email:data.email, 
            role: data.role
        }
        res.data = userData;      
    } catch (err) {            
        err.status = 404;
        err.message = 'User not found';
        res.err = err      
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;