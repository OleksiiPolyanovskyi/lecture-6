const { Router } = require('express');
const PostService = require('../services/PostService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('', (req, res, next) => {
    try {
        const posts = PostService.getAllPosts();
        res.data = posts
      }catch(err) {
        err.status = 404;        
        err.message = 'Posts were not found';
        res.err = err        
      } finally {
        next();
      }  
}, responseMiddleware)

router.post('', (req, res, next) => {
    try {
      req.body.likesCount = Array.from(req.body.likesCount)
      const post = PostService.createPost(req.body);
      res.data = post;
      
      }catch(err) {
        err.status = 400;       
        err.message = 'Post entity to create is not valid';
        res.err = err        
      } finally {
        next();
      }  
}, responseMiddleware)

router.get('/:id', (req, res, next) => {    
    try {
        const post = PostService.getPostById(req.params.id);
        res.data = post;
      }catch(err) {
        err.status = 404;        
        err.message = 'Post was not found';
        res.err = err       
      } finally {
        next();
      }  
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {    
    try {
        const post = PostService.removePost(req.params.id);
        res.data = {message: 'Post was removed'};
      }catch(err) {
        err.status = 404;        
        err.message = 'Failed to remove post';
        res.err = err                
      } finally {
        next();
      }  
}, responseMiddleware)

router.put('/:id', (req, res, next) => {     
    try {
      req.body.likesCount = Array.from(req.body.likesCount)    
      const post = PostService.updatePost(req.params.id, req.body);
      res.data = post;      
      }catch(err) {
        err.status = 400;       
        err.message = 'Failed to update post';
        res.err = err                
      } finally {
        next();
      }  
}, responseMiddleware)

router.put('/like/:id', (req, res, next) => {     
  try {  
    PostService.likePost(req.params.id, req.body.userId);        
    }catch(err) {
      err.status = 400;      
      err.message = 'Failed to like post';
      res.err = err             
    } finally {
      next();
    }  
}, responseMiddleware)

module.exports = router;