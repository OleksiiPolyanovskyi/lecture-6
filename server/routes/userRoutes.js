const { Router } = require('express');
const UserService = require('../services/userService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('', (req, res, next) => {
    try {
        const users = UserService.getAllUsers();        
        res.data = users;
      }catch(err) {
        err.status = 404;       
        err.message = 'Users were not found';
        res.err = err      
      } finally {
        next();
      }  
}, responseMiddleware)

router.post('', (req, res, next) => {
  try {
    const user = UserService.createNewUser(req.body);
    res.data = user;
    
    }catch(err) {
      err.status = 400;    
      err.message = 'Post entity to create is not valid';
      res.err = err       
    } finally {
      next();
    }  
}, responseMiddleware)

router.get('/:id', (req, res, next) => {    
    try {
        const user = UserService.getUserById(req.params.id);
        res.data = user;
      }catch(err) {
        err.status = 404;       
        err.message = 'User was not found';
        res.err = err               
      } finally {
        next();
      }  
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {    
    try {
        const user = UserService.removeUser(req.params.id);
        res.data = {message: 'User was removed'};
      }catch(err) {
        err.status = 400;       
        err.message = 'Failed to remove user';
        res.err = err                
      } finally {
        next();
      }  
}, responseMiddleware)

router.put('/:id', (req, res, next) => {     
    try {          
      const user = UserService.updateUser(req.params.id, req.body);
      res.data = user;      
      }catch(err) {
        err.status = 400;        
        err.message = 'Failed to update user';
        res.err = err               
      } finally {
        next();
      }  
}, responseMiddleware)

module.exports = router;