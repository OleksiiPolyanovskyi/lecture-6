const { PostRepository } = require('../repositories/postRepository');

class PostService {    
    getAllPosts() {
        const posts = PostRepository.getAll();
        if(!posts) {
            return null;
        }
        return posts;
    }
    createPost(post) {
        const newPost = PostRepository.create(post)
        if(!newPost) {
            return null;
        } 
        return newPost;
    }
    getPostById(id) {
        const post = PostRepository.getOne({id});
        if(!post) {
            throw new Error();
        }
        return post;
    }
    removePost(id) {
        const post = PostRepository.delete(id);
        if(post.length === 0) {            
            throw new Error();
        }
        return post;
    }

    updatePost(id, params) {
        const post = PostRepository.update(id, params);
        if(!post.id) {            
            throw new Error();
        }
        return post;
    }
    likePost(postId, userId){
        const post = PostRepository.getOne({id: postId});       
        if(!post.id) {            
            throw new Error();
        }
        if (!post.likesCount.includes(userId)){
            post.likesCount.push(userId)
            PostRepository.update(postId, post)
        }       
    }
}

module.exports = new PostService();